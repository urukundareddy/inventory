package com.urukunda.inventory.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("inventory")
public class Inventory {

    private String productName;
    private int productCount;
    private double productPrice;
}
