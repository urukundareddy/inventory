package com.urukunda.inventory.controller;


import com.urukunda.inventory.model.Inventory;
import com.urukunda.inventory.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
public class InventoryController {

    @Autowired
    private InventoryService inventoryService;

    @PostMapping("/inventory")
    public Inventory saveInventory(@RequestBody Inventory inventory){
        return inventoryService.saveInventory(inventory);
    }

    @GetMapping("/inventory/{productName}")
    public Optional<Inventory> findInventoryByName(@PathVariable String productName){
        return inventoryService.findInventoryById(productName);
    }
}
