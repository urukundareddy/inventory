package com.urukunda.inventory.controller;

import com.urukunda.inventory.model.Inventory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;

@FeignClient("inventory")
public interface IInventory {

    @PostMapping("/inventory")
    public Inventory saveInventory(@RequestBody Inventory inventory);

    @GetMapping("/inventory/{productName}")
    public Optional<Inventory> findInventoryByName(@PathVariable String productName);
}
