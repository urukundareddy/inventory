package com.urukunda.inventory.service;

import com.urukunda.inventory.model.Inventory;
import com.urukunda.inventory.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InventoryService {
    @Autowired
    private InventoryRepository inventoryRepository;

    public Inventory saveInventory(Inventory inventory){
        return inventoryRepository.save(inventory);
    }

    public Optional<Inventory> findInventoryById(String productName){
        return inventoryRepository.findById(productName);
    }
}
