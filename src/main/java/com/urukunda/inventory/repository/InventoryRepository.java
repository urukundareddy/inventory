package com.urukunda.inventory.repository;

import com.urukunda.inventory.model.Inventory;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

@Repository
public interface InventoryRepository extends MongoRepository<Inventory, String> {
}
